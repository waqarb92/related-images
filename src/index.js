import 'normalize.css/normalize.css';
import './styles/index.scss';
import data from './data/data.json';



// DOM selectors
const nodes = {
    imageWrapper: document.querySelector('#imagesWrapper'),
    imageLabels: document.querySelector('#imageLabels'),
    selectedImage: document.querySelector('#selectedImage'),
    relatedImages: document.querySelector('#relatedImages'),
    imageElement: '' // imageElement items do not exist on load
};

// Click event listeners
function setClickEvents() {
    // Add a click event listener to all imageElement items
    for (let i = 0; i < nodes.imageElement.length; i++) {
        nodes.imageElement[i].addEventListener("click", function (e) {
            e.preventDefault();
            selectImage(nodes.imageElement[i]);
        });
    }
}

// Select clicked image and call function to load related images
function selectImage(selected) {
    let img = selected.dataset.img;

    // Loop through all image elements and remove selected class and only add it to the clicked item
    for (let i = 0; i < nodes.imageElement.length; i++) {
        nodes.imageElement[i].classList.remove('selected');
    }
    selected.classList.add('selected')

    let fullScore = ""; // Empty variable for score html

    // Loop through all labels for the selected image and add the correct HTML to full score string.
    for (const label of data[img]) {
        fullScore += getScoreTemplate(label.description, label.score);
    }

    // Add selected image and labels to containers
    nodes.selectedImage.innerHTML = `<img src="${require(`./assets/${img}`)}" />`
    nodes.imageLabels.innerHTML = fullScore;
    loadRelated(img);
}

function loadRelated(img) {
    const relatedBy = 5 // This is the number of labels that will be used from the selected image to match related images
    const relatedLabels = getLabels(relatedBy, img)
    let relatedItems = [];
    let relatedHTML = "";

    // For each item within each image object, loop through all descriptions and increment matches if any are found
    // Once atleast 3 matches have been found add that item to the relatedItems array and return
    Object.keys(data).forEach(function (item) {
        let matches = 0;
        for (let i = 0; i < data[item].length; i++) {
            if (relatedLabels.includes(data[item][i].description)) {
                matches++
            }
            if (matches >= 3) {
                relatedItems.push(item)
                return;
            }
        }
    });

    // Loop through related items and add all but the currently selected item to the related HTML string
    if (relatedItems.length > 1) {
        for (const item of relatedItems) {
            if (item !== img) {
                relatedHTML += `<img class="Image_related" src="${require(`./assets/${item}`)}" />`;
            }
        }
    } else {
        if (relatedItems[0] === img) {
            relatedHTML = '<p class="Error">No related images found</p>';
        }
    }


    // Insert HTML into relatedImages element
    nodes.relatedImages.innerHTML = relatedHTML;
}

// Based on the number given, x amount of labels will be returned from the given element
// Labels are in order of score (highest to lowest) so they are taken from the top as these will have the most accuracy
function getLabels(number, img) {
    let labels = [];
    for (let i = 0; i < number; i++) {
        labels.push(data[img][i].description);
    }
    return labels;
}

function loadImages() {
    let fullList = "";
    Object.keys(data).map(image => {
        fullList += getImageTemplate(image);
    });
    nodes.imageWrapper.innerHTML = fullList;

    nodes.imageWrapper.classList.add("loaded");

    // Assign imageElement after they have been loaded onto the page
    nodes.imageElement = document.querySelectorAll('.Images-item');
    setClickEvents();

}

function getScoreTemplate(label, score) {
    return `
  <div class="Image_score">
    <div class="Image_score_label">${label}</div>
    <div class="Image_score_value">${score.toFixed(2)}</div>
  </div>
  `;
}

function getImageTemplate(Image) {
    return `
      <a href="#" data-img="${Image}" class="Images-item">
      <img src="${require(`./assets/${Image}`)}" />
      </a>
    `;
}

// Initialise the script
const init = () => {
    loadImages();
};


// Wait for the DOM to be ready
document.addEventListener("DOMContentLoaded", function () {
    init();
});
